use async_trait::async_trait;
use futures::Stream;
use serde_json::Value;
use std::borrow::Cow;
use std::collections::BTreeMap;
use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::pin::Pin;
use thiserror::Error;
use uuid::Uuid;

#[async_trait]
pub trait Cells: Debug + Clone {
    type Error: Display;
    type Table: Table<Error = Self::Error>;

    /// Get a metadata value.
    async fn meta(&self, name: &str) -> Result<Option<String>, Self::Error>;

    /// Set a metadata value.
    async fn meta_set(&self, name: &str, value: Option<&str>) -> Result<(), Self::Error>;

    /// List all tables.
    async fn tables(&self) -> Result<Vec<Self::Table>, Self::Error>;

    /// Fetch a table by name.
    async fn table_by_name(&self, name: &str) -> Result<Option<Self::Table>, Self::Error>;

    /// Fetch a table by ID.
    async fn table(
        &self,
        id: <Self::Table as Table>::Id,
    ) -> Result<Option<Self::Table>, Self::Error>;

    /// Create a new table.
    async fn table_create(
        &self,
        name: &str,
        display: Option<&str>,
    ) -> Result<Self::Table, Self::Error>;

    async fn events(&self) -> Pin<Box<dyn Stream<Item = Result<(), ()>> + Send>>;
}

pub trait TableInfo: Clone + Debug {
    fn name(&self) -> String;
    fn display(&self) -> Option<String>;
    fn description(&self) -> Option<String>;
}

#[async_trait]
pub trait Table: Debug + Clone {
    type Error: Display;
    type Column: Column<Error = Self::Error>;
    type Info: TableInfo;
    type Id: Debug + Clone + PartialEq + Eq + PartialOrd + Ord + Hash;

    /// Get id of this table
    fn id(&self) -> Self::Id;

    /// Get info
    async fn info(&self) -> Result<Self::Info, Self::Error>;

    /// Get columns
    async fn columns(&self) -> Result<Vec<Self::Column>, Self::Error>;

    /// Get individual column
    async fn column_by_name(&self, name: &str) -> Result<Option<Self::Column>, Self::Error>;

    /// Create a new column
    async fn column_create(&self, name: &str) -> Result<Self::Column, Self::Error>;

    /// Set name
    async fn name_set(&self, name: &str) -> Result<(), Self::Error>;

    /// Set label
    async fn display_set(&self, name: Option<&str>) -> Result<(), Self::Error>;

    /// Set description
    async fn description_set(&self, name: Option<&str>) -> Result<(), Self::Error>;

    /// Insert a single row
    async fn row_insert(&self, data: BTreeMap<String, Value>) -> Result<(), Self::Error>;
}

#[async_trait]
pub trait Column: Debug + Clone {
    type Error: Display;
    type Id: Debug + Clone + PartialEq + Eq + PartialOrd + Ord + Hash;

    /// Get id of this column
    fn id(&self) -> Self::Id;

    /// Get name
    async fn name(&self) -> Result<String, Self::Error>;
    /// Get label
    async fn label(&self) -> Result<String, Self::Error>;
    /// Get description
    async fn description(&self) -> Result<String, Self::Error>;

    /// Set name
    async fn set_name(&self, name: &str) -> Result<(), Self::Error>;
    /// Set label
    async fn set_label(&self, name: &str) -> Result<(), Self::Error>;
    /// Set description
    async fn set_description(&self, name: &str) -> Result<(), Self::Error>;
}
