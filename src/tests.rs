use sqlx::AnyPool;

#[tokio::test]
async fn test_migrations() {
    let pool = AnyPool::connect("sqlite://:memory:").await.unwrap();
    sqlx::migrate!().run(&pool).await.unwrap();
}

#[tokio::test]
async fn test_simple() {
    /*
    let cells = TempCells::new();

    // define a data type called euros
    cells.type_create("euros", TypeDef {
        storage_type: StorageType::INTEGER,
        validator: script!{
            (gt (input) 0)
        },
        formatter: script!{
            (format "{}.{}€" (div (input) 100) (mod (input) 100))
        },
    }).await.unwrap();

    // create a table called prices
    let table = cells.table_create("prices", TableDef {
        columns: columns!{
            name: "string" unique,
            value: "euros",
        }
    }).await?;

    table.row_insert(row!{
        name: "test",
        value: "0€",
    }).await?;

    table.row_insert(row!{
        name: "upset",
        value: "8.99€",
    }).await?;
    */
}
