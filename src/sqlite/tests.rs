use super::*;
use crate::cells::{Cells, Table, TableInfo};
use sqlx::AnyPool;

#[tokio::test]
async fn migrations() {
    let pool = AnyPool::connect("sqlite://:memory:").await.unwrap();
    sqlx::migrate!().run(&pool).await.unwrap();
}

#[tokio::test]
async fn cells_meta() {
    let cells = SqliteCells::memory().await.unwrap();
    assert_eq!(cells.meta("hello").await.unwrap(), None);
    cells.meta_set("hello", Some("123")).await.unwrap();
    assert_eq!(cells.meta("hello").await.unwrap(), Some("123".into()));
    cells.meta_set("hello", None).await.unwrap();
    assert_eq!(cells.meta("hello").await.unwrap(), None);
}

#[tokio::test]
async fn cells_table() {
    let cells = SqliteCells::memory().await.unwrap();
    let table = cells
        .table_create("variables", Some("Variables"))
        .await
        .unwrap();

    let tables = cells.tables().await.unwrap();
    assert_eq!(tables.len(), 1);
    assert_eq!(tables[0], table);

    let table_not_exists = cells.table_by_name("not_exists").await.unwrap();
    assert!(table_not_exists.is_none());

    let table_by_name = cells.table_by_name("variables").await.unwrap().unwrap();
    assert_eq!(table_by_name, table);
}

#[tokio::test]
async fn cells_table_info() {
    let cells = SqliteCells::memory().await.unwrap();
    let table = cells
        .table_create("variables", Some("Variables"))
        .await
        .unwrap();
    let info = table.info().await.unwrap();
    assert_eq!(info.name(), "variables");
    assert_eq!(info.display(), Some("Variables".into()));
    assert_eq!(info.description(), None);

    table.name_set("other").await.unwrap();
    table.display_set(Some("Other")).await.unwrap();
    table.description_set(Some("Other stuff")).await.unwrap();

    let info = table.info().await.unwrap();
    assert_eq!(info.name(), "other");
    assert_eq!(info.display(), Some("Other".into()));
    assert_eq!(info.description(), Some("Other stuff".into()));
}

#[tokio::test]
async fn can_fetch_empty_columns() {
    let cells = SqliteCells::memory().await.unwrap();
    let table = cells
        .table_create("variables", Some("Variables"))
        .await
        .unwrap();
    let columns = table.columns().await.unwrap();
    assert!(columns.is_empty());

    let column = table.column_by_name("col").await.unwrap();
    assert_eq!(column, None);
}

#[tokio::test]
async fn can_create_columns() {
    let cells = SqliteCells::memory().await.unwrap();
    let table = cells
        .table_create("variables", Some("Variables"))
        .await
        .unwrap();
    let columns = table.columns().await.unwrap();
    assert!(columns.is_empty());

    let key_column = table.column_create("name").await.unwrap();
    let columns = table.columns().await.unwrap();
    assert_eq!(columns, vec![key_column.clone()]);

    let value_column = table.column_create("value").await.unwrap();
    let columns = table.columns().await.unwrap();
    assert_eq!(columns, vec![key_column, value_column]);
}
