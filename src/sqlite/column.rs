use crate::cells::*;
use crate::sqlite::SqliteCells;
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use sqlx::AnyPool;
use std::borrow::Cow;
use std::collections::BTreeMap;

#[derive(Clone, Debug)]
pub struct SqliteColumn {
    cells: SqliteCells,
    id: i64,
}

impl SqliteColumn {
    pub fn new(cells: SqliteCells, id: i64) -> Self {
        SqliteColumn { cells, id }
    }
}

impl PartialEq for SqliteColumn {
    fn eq(&self, rhs: &SqliteColumn) -> bool {
        self.id.eq(&rhs.id)
    }
}

impl Eq for SqliteColumn {}

#[async_trait]
impl Column for SqliteColumn {
    type Error = anyhow::Error;
    type Id = i64;

    fn id(&self) -> Self::Id {
        self.id
    }

    async fn name(&self) -> Result<String, Self::Error> {
        unimplemented!()
    }

    async fn label(&self) -> Result<String, Self::Error> {
        unimplemented!()
    }

    async fn description(&self) -> Result<String, Self::Error> {
        unimplemented!()
    }

    async fn set_name(&self, name: &str) -> Result<(), Self::Error> {
        unimplemented!()
    }

    async fn set_label(&self, name: &str) -> Result<(), Self::Error> {
        unimplemented!()
    }

    async fn set_description(&self, name: &str) -> Result<(), Self::Error> {
        unimplemented!()
    }
}
