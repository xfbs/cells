use super::SqliteTable;
use crate::cells::*;
use async_trait::async_trait;
use futures::Stream;
use serde::{Deserialize, Serialize};
use sqlx::{pool::PoolConnection, query, query_as, Any, AnyConnection, AnyPool, Transaction};
use std::borrow::Cow;
use std::collections::BTreeMap;
use std::ops::DerefMut;
use std::pin::Pin;
use std::str::FromStr;
use std::sync::Arc;
use tokio::sync::{Mutex, MutexGuard};
use uuid::Uuid;

#[derive(Clone, Debug)]
pub struct SqliteCells {
    pub(crate) context: Context,
}

#[derive(Clone, Debug)]
pub enum Context {
    Pool(AnyPool),
    Transaction(Arc<Mutex<Transaction<'static, Any>>>),
}

impl Context {
    pub async fn executor(&self) -> Result<Executor<'_>, sqlx::Error> {
        use Context::*;
        match self {
            Pool(pool) => Ok(Executor::Connection(pool.acquire().await?)),
            Transaction(transaction) => Ok(Executor::Transaction(transaction.lock().await)),
        }
    }
}

#[derive(Debug)]
pub enum Executor<'a> {
    Connection(PoolConnection<Any>),
    Transaction(MutexGuard<'a, Transaction<'static, Any>>),
}

impl<'a> Executor<'a> {
    pub fn executor(&mut self) -> &mut AnyConnection {
        match self {
            Executor::Connection(conn) => conn.deref_mut(),
            Executor::Transaction(guard) => guard.deref_mut().deref_mut(),
        }
    }
}

impl SqliteCells {
    pub async fn memory() -> Result<Self, sqlx::Error> {
        let pool = AnyPool::connect("sqlite://:memory:").await.unwrap();
        sqlx::migrate!().run(&pool).await.unwrap();
        let context = Context::Pool(pool.clone());
        Ok(SqliteCells { context })
    }

    pub async fn transaction(&self) -> Result<Self, sqlx::Error> {
        let pool = match &self.context {
            Context::Pool(pool) => pool,
            Context::Transaction(transaction) => unreachable!(),
        };
        let transaction = pool.begin().await?;
        let context = Context::Transaction(Arc::new(Mutex::new(transaction)));
        Ok(SqliteCells { context })
    }

    pub async fn executor(&self) -> Result<Executor<'_>, sqlx::Error> {
        self.context.executor().await
    }
}

#[async_trait]
impl Cells for SqliteCells {
    type Error = anyhow::Error;
    type Table = super::SqliteTable;

    async fn meta(&self, name: &str) -> Result<Option<String>, Self::Error> {
        let mut executor = self.context.executor().await?;
        let result: Option<(String,)> = query_as("SELECT value FROM cells_meta WHERE name = ?")
            .bind(name)
            .fetch_optional(executor.executor())
            .await?;
        Ok(result.map(|(r,)| r))
    }

    async fn tables(&self) -> Result<Vec<Self::Table>, Self::Error> {
        let mut executor = self.context.executor().await?;
        let tables: Vec<(i64,)> = query_as("SELECT id FROM cells_table")
            .fetch_all(executor.executor())
            .await?;
        let tables: Vec<_> = tables
            .iter()
            .map(|r| SqliteTable::new(self.clone(), r.0))
            .collect();
        Ok(tables)
    }

    async fn table_by_name(&self, name: &str) -> Result<Option<Self::Table>, Self::Error> {
        let mut executor = self.context.executor().await?;
        let table_id: Option<(i64,)> = query_as("SELECT id FROM cells_table WHERE name = ?")
            .bind(name)
            .fetch_optional(executor.executor())
            .await?;
        match table_id {
            Some(table_id) => Ok(Some(SqliteTable::new(self.clone(), table_id.0))),
            None => Ok(None),
        }
    }

    async fn table(
        &self,
        id: <Self::Table as Table>::Id,
    ) -> Result<Option<Self::Table>, Self::Error> {
        Ok(Some(SqliteTable::new(self.clone(), id)))
    }

    async fn meta_set(&self, name: &str, value: Option<&str>) -> Result<(), Self::Error> {
        let mut executor = self.context.executor().await?;
        if let Some(value) = value {
            query("INSERT OR REPLACE INTO cells_meta(name, value) VALUES (?, ?)")
                .bind(name)
                .bind(value)
                .execute(executor.executor())
                .await?;
        } else {
            query("DELETE FROM cells_meta WHERE name = ?")
                .bind(name)
                .execute(executor.executor())
                .await?;
        }
        Ok(())
    }

    async fn table_create(
        &self,
        name: &str,
        display: Option<&str>,
    ) -> Result<Self::Table, Self::Error> {
        let mut executor = self.context.executor().await?;
        let result = query("INSERT INTO cells_table(name, display) VALUES (?, ?)")
            .bind(name)
            .bind(display)
            .execute(executor.executor())
            .await?;
        Ok(SqliteTable::new(
            self.clone(),
            result.last_insert_id().unwrap(),
        ))
    }

    async fn events(&self) -> Pin<Box<dyn Stream<Item = Result<(), ()>> + Send>> {
        unimplemented!()
    }
}
