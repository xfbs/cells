use crate::cells::*;
use crate::sqlite::{SqliteCells, SqliteColumn};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use sqlx::AnyPool;
use sqlx::{query, query_as, FromRow};
use std::borrow::Cow;
use std::collections::BTreeMap;
use std::fmt::Display;
use uuid::Uuid;

#[derive(Clone, Debug, FromRow)]
pub struct SqliteTableInfo {
    name: String,
    description: Option<String>,
    display: Option<String>,
}

impl TableInfo for SqliteTableInfo {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn display(&self) -> Option<String> {
        self.display.clone()
    }

    fn description(&self) -> Option<String> {
        self.description.clone()
    }
}

#[derive(Clone, Debug)]
pub struct SqliteTable {
    pub(crate) cells: SqliteCells,
    pub(crate) id: i64,
}

impl PartialEq for SqliteTable {
    fn eq(&self, rhs: &SqliteTable) -> bool {
        self.id.eq(&rhs.id)
    }
}

impl Eq for SqliteTable {}

impl SqliteTable {
    pub fn new(cells: SqliteCells, id: i64) -> Self {
        SqliteTable { cells, id }
    }
}

#[async_trait]
impl Table for SqliteTable {
    type Error = anyhow::Error;
    type Column = super::SqliteColumn;
    type Info = SqliteTableInfo;
    type Id = i64;

    fn id(&self) -> Self::Id {
        self.id
    }

    async fn info(&self) -> Result<Self::Info, Self::Error> {
        let mut executor = self.cells.executor().await?;
        let info: SqliteTableInfo = query_as("SELECT * FROM cells_table WHERE id = ?")
            .bind(self.id)
            .fetch_one(executor.executor())
            .await?;
        Ok(info)
    }

    async fn columns(&self) -> Result<Vec<Self::Column>, Self::Error> {
        let mut executor = self.cells.executor().await?;
        let tables: Vec<(i64,)> = query_as("SELECT id FROM cells_column WHERE cells_table = ?")
            .bind(self.id)
            .fetch_all(executor.executor())
            .await?;
        let columns: Vec<_> = tables
            .iter()
            .map(|r| SqliteColumn::new(self.cells.clone(), r.0))
            .collect();
        Ok(columns)
    }

    async fn column_by_name(&self, name: &str) -> Result<Option<Self::Column>, Self::Error> {
        let mut executor = self.cells.executor().await?;
        let column_id: Option<(i64,)> =
            query_as("SELECT id FROM cells_column WHERE name = ? AND cells_table = ?")
                .bind(name)
                .bind(self.id)
                .fetch_optional(executor.executor())
                .await?;
        match column_id {
            Some(column_id) => Ok(Some(SqliteColumn::new(self.cells.clone(), column_id.0))),
            None => Ok(None),
        }
    }

    async fn column_create(&self, name: &str) -> Result<Self::Column, Self::Error> {
        let mut executor = self.cells.executor().await?;
        let result = query("INSERT INTO cells_column(name, cells_table) VALUES (?, ?)")
            .bind(name)
            .bind(self.id)
            .execute(executor.executor())
            .await?;
        Ok(SqliteColumn::new(
            self.cells.clone(),
            result.last_insert_id().unwrap(),
        ))
    }

    async fn name_set(&self, name: &str) -> Result<(), Self::Error> {
        let mut executor = self.cells.executor().await?;
        query("UPDATE cells_table SET name = ? WHERE id = ?")
            .bind(name)
            .bind(self.id)
            .execute(executor.executor())
            .await?;
        Ok(())
    }

    async fn display_set(&self, name: Option<&str>) -> Result<(), Self::Error> {
        let mut executor = self.cells.executor().await?;
        query("UPDATE cells_table SET display = ? WHERE id = ?")
            .bind(name)
            .bind(self.id)
            .execute(executor.executor())
            .await?;
        Ok(())
    }

    async fn description_set(&self, name: Option<&str>) -> Result<(), Self::Error> {
        let mut executor = self.cells.executor().await?;
        query("UPDATE cells_table SET description = ? WHERE id = ?")
            .bind(name)
            .bind(self.id)
            .execute(executor.executor())
            .await?;
        Ok(())
    }

    async fn row_insert(&self, data: BTreeMap<String, Value>) -> Result<(), Self::Error> {
        for column in &self.columns().await? {
        }
        Ok(())
    }
}
