-- metadata, key value store for stuff like version, plugins
CREATE TABLE cells_meta(
    name TEXT NOT NULL PRIMARY KEY,
    value BLOB
);

-- list of tables
-- each table has an ID (internal), a name (unique) and a display (what is shown).
CREATE TABLE cells_table(
    -- internal, stable identifier
    id INTEGER NOT NULL PRIMARY KEY,
    -- name for this table (must be unique)
    name TEXT NOT NULL UNIQUE,
    -- optional human-friendly name
    display TEXT,
    -- optional description
    description TEXT
);

-- list of data types
CREATE TABLE cells_type(
    id INTEGER NOT NULL PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    validater INTEGER REFERENCES cells_expression(id) ON DELETE CASCADE,
    formatter INTEGER REFERENCES cells_expression(id) ON DELETE CASCADE,
    parser INTEGER REFERENCES cells_expression(id) ON DELETE CASCADE
);

-- an expression
CREATE TABLE cells_expression(
    id INTEGER NOT NULL PRIMARY KEY,
    func TEXT NOT NULL,
    lhs INTEGER REFERENCES cells_expression(id) ON DELETE CASCADE,
    rhs INTEGER REFERENCES cells_expression(id) ON DELETE CASCADE,
    column INTEGER REFERENCES cells_column(id) ON DELETE CASCADE,
    cells_table INTEGER REFERENCES cells_table(id) ON DELETE CASCADE,
    value TEXT
);

-- list of table columns
CREATE TABLE cells_column(
    id INTEGER NOT NULL PRIMARY KEY,
    cells_table INTEGER NOT NULL REFERENCES cells_table(id) ON DELETE CASCADE,
    name TEXT NOT NULL UNIQUE,
    display TEXT,
    description TEXT,
    cells_type INTEGER REFERENCES cells_type(id)
);

-- list of table columns
CREATE TABLE cells_row(
    id UUID NOT NULL PRIMARY KEY,
    cells_table INTEGER NOT NULL REFERENCES cells_table(id) ON DELETE CASCADE
);

-- list of cells in this document
CREATE TABLE cells_cell(
    cells_row UUID NOT NULL REFERENCES cells_row(id) ON DELETE CASCADE,
    cells_column UUID NOT NULL REFERENCES cells_column(id) ON DELETE CASCADE,
    value BLOB,
    PRIMARY KEY (cells_row, cells_column)
);

-- list of scripts in this document
-- scripts can be defined in some language and run as a response to certain events.
CREATE TABLE cells_scripts(
    id UUID NOT NULL PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    display TEXT,
    language TEXT NOT NULL,
    body TEXT
);
